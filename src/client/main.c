/*
 * Infiltrator - simple top-down theft game
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

int main(void)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init(): Couldn't initialize: %s", SDL_GetError());
		return -1;
	}

	SDL_Window *win = SDL_CreateWindow("Infiltrator", 100, 100, 640, 480,
		SDL_WINDOW_SHOWN);

	if (win == NULL) {
		printf("SDL_CreateWindow(): Couldn't create: %s", SDL_GetError());
		return -1;
	}

	SDL_Delay(2000);

	return 0;
}
