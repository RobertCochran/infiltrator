/*
 * Infiltrator - simple top-down theft game
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "net.h"

int init_network(int *sock, struct addrinfo *serv)
{
	struct addrinfo hints, *res, *p;
	int status;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	status = getaddrinfo(NULL, "12397", &hints, &res);

	if (status != 0) {
		fprintf(stderr, "init_network() :"
				" could not get address info : %s\n",
				gai_strerror(status));
		return -1;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		*sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);

		if (*sock == -1) {
			perror("init_network() : socket ");
			continue;
		}

		int yes = 1;

		status = setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR,
				&yes, sizeof(yes));

		if (status == -1) {
			close(*sock);
			perror("init_network() : "
					"could not set socket option ");
			continue;
		}

		status = bind(*sock, p->ai_addr, p->ai_addrlen);

		if (status == -1) {
			close(*sock);
			perror("init_network() : bind ");
			continue;
		}

		break;
	}

	if (p == NULL) {
		perror("init_network() : could not bind ");
		return -1;
	}

	serv = p;

	freeaddrinfo(res);

	return 0;
}

void *get_addr(struct sockaddr *a)
{
	/* Handle IPv4 address */
	if (a->sa_family == AF_INET)
		return &(((struct sockaddr_in *)a)->sin_addr);

	return &(((struct sockaddr_in6 *)a)->sin6_addr);
}

JSON_Value *get_message(struct sockaddr *their_addr, int sock, char *buffer,
	size_t buf_len)
{
	socklen_t addr_len = sizeof(*their_addr);

	int got = recvfrom(sock, (void *) buffer, (buf_len - 1), 0,
		their_addr, &addr_len);

	if (got == -1) return NULL;

	buffer[got] = '\0';

	return json_parse_string(buffer);
}
