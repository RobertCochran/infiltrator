/*
 * Infiltrator - simple top-down theft game
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef INFILTRATOR_SERV_PLAYER_H
#define INFILTRATOR_SERV_PLAYER_H

#define MAX_PLAYERS 32

/* For struct addrinfo */
#include <netdb.h>

struct Player_t {
	struct addrinfo *player_ip;

	unsigned int player_x;
	unsigned int player_y;

	int player_x_vel;
	int player_y_vel;
};

struct Player_t **init_player_list(void);

int add_player(struct Player_t **list, struct addrinfo *ip);

int remove_player(struct Player_t **list, int id);

#endif
