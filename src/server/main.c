/*
 * Infiltrator - simple top-down theft game
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parson.h"
#include "net.h"
#include "player.h"
#include "version.h"

int main(void)
{
	printf("Infiltrator Server %d.%d.%d starting up...\n",
	SERVER_MAJOR_VERSION, SERVER_MINOR_VERSION, SERVER_PATCH_VERSION);

	int socket = -1;
	struct addrinfo *serv = NULL;

	if (init_network(&socket, serv) != 0) return -1;

	struct sockaddr_storage their_addr;

	struct Player_t **players = init_player_list();

	if (players == NULL) {
		perror("Could not allocate memory");
		return -1;
	}

	size_t buf_len = 1024;
	char *read_buffer = malloc(buf_len * sizeof(char));
	char *write_buffer = malloc(buf_len * sizeof(char));

	while (1) {
		memset(write_buffer, 0, buf_len);

		JSON_Value *root = get_message((struct sockaddr *)&their_addr,
			socket, read_buffer, buf_len);

		if (root == NULL) {
			perror("recvfrom(): ");
			return -1;
		}

		char s[INET6_ADDRSTRLEN];

		inet_ntop(their_addr.ss_family,
			get_addr((struct sockaddr *)&their_addr), s, sizeof(s));

		printf("\nGot a packet from %s\n", s);

		if (json_value_get_type(root) != JSONObject) {
			printf("[Error] Not a valid JSON object, kthx\n");
			continue;
		}

		JSON_Object *real_value = json_value_get_object(root);

		int client_major = (int) json_object_dotget_number(real_value,
			"version.major");
		int client_minor = (int) json_object_dotget_number(real_value,
			"version.minor");
		int client_patch = (int) json_object_dotget_number(real_value,
			"version.patch");


		printf("Reported version number is \"%d.%d.%d\"\n",
			client_major, client_minor, client_patch);

		if (client_major != SERVER_MAJOR_VERSION) {
			printf("[Error] Major version mis-match from %s, "
				"refusing join request\n", s);
		} else if (client_minor != SERVER_MINOR_VERSION) {
			printf("[Warning] Minor version mis-match from %s, "
				"something may break\n", s);
		} else if (client_patch != SERVER_PATCH_VERSION) {
			printf("[Info] Patch version mis-match from %s, "
				"but it should work\n", s);
		}

		char *msg_type = (char *) json_object_get_string(real_value,
			"type");

		if (strcmp(msg_type, "hello") == 0) {
			if (client_major != SERVER_MAJOR_VERSION) continue;

			int id = add_player(players,
				(struct sockaddr *) &their_addr);

			if (id == -1) {
				printf("Couldn't add %s to player list\n\n", s);
			} else {
				printf("Added %s as id %d\n\n",	s, id);
			}
		}

		if (strcmp(msg_type, "bye") == 0) {
			int i = match_player_id(players,
				(struct sockaddr *) &their_addr);

			if (i == -1) {
				printf("[Warning] Got a \"bye\" message from "
					"%s, who is not in the player list. "
					"Ignoring...\n", s);
			} else {
				remove_player(players, i);
				printf("[Info] %s left the game\n", s);
			}
		}

		if (strcmp(msg_type, "read") == 0) {
			int id = (int) json_object_get_number(real_value, "id");

			if (id < 0 || id >= MAX_PLAYERS) {
				printf("Received ID is out of range.\n\n");
				continue;
			}

			if (players[id] == NULL) {
				printf("Received ID is not a player.\n\n");
				continue;
			}

			printf("Info for ID %d : \n\n", id);
			printf("X : %d\nY : %d\n\n", players[id]->player_x,
				players[id]->player_y);
			printf("X Vel : %d\nY Vel : %d\n\n",
				players[id]->player_x_vel,
				players[id]->player_y_vel);
		}

		if (strcmp(msg_type, "update") == 0) {
			int id = (int) json_object_get_number(real_value, "id");

			if (id < 0 || id >= MAX_PLAYERS) {
				printf("Received ID is out of range.\n\n");
				continue;
			}

			if (players[id] == NULL) {
				printf("Received ID is not a player.\n\n");
				continue;
			}

			/* If it's really ever necessary, one can do sanity/
			 * cheat checking here */
			
			players[id]->player_x = json_object_dotget_number(
				real_value, "value.x");
			players[id]->player_y = json_object_dotget_number(
				real_value, "value.y");
			players[id]->player_x_vel = json_object_dotget_number(
				real_value, "value.x_vel");
			players[id]->player_y_vel = json_object_dotget_number(
				real_value, "value.y_vel");

		}
	}

	return 0;
}
