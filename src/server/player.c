/*
 * Infiltrator - simple top-down theft game
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <stdlib.h>
#include <string.h>

#include "player.h"
#include "net.h"

struct Player_t **init_player_list(void)
{
	struct Player_t **list = malloc(MAX_PLAYERS
			* sizeof(struct Player_t *));
	memset(list, 0, MAX_PLAYERS * sizeof(struct Player_t *));
	return list;
}

int add_player(struct Player_t **list, struct sockaddr *ip)
{	
	/* Go through the list and find the first unused */
	int i;
	for (i = 0; list[i] != NULL && i < MAX_PLAYERS; i++);

	if (i >= MAX_PLAYERS) return -1;

	list[i] = malloc(sizeof(struct Player_t));

	list[i]->player_ip = ip;
	list[i]->player_x = 0;
	list[i]->player_y = 0;
	list[i]->player_x_vel = 0;
	list[i]->player_y_vel = 0;

	return i;
}

int remove_player(struct Player_t **list, int id)
{
	if (id < 0 || id > MAX_PLAYERS) return 1;

	free(list[id]);
	memset((void *) list[id], 0, sizeof(struct Player_t *));

	return 0;
}

int match_player_id(struct Player_t **list, struct sockaddr *a)
{
	int i;

	for (i = 0; i != MAX_PLAYERS && list[i]; i++) {
		/* Direct struct-to-struct comparsions aren't possible,
 		 * so matching address members are good enough */

		if (get_addr(list[i]->player_ip) == get_addr(a))
			return i;
	}

	return -1;
}
